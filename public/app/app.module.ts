import {NgModule} from '@angular/core';
import {AppComponent} from '../components/main/app.component';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports : [BrowserModule, FormsModule],
  declarations : [AppComponent], //All component will go here
  bootstrap : [AppComponent]
  
})

export class AppModule{}