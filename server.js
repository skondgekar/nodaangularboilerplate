//setup
var express = require('express');
var session = require('express-session');
var router = express.Router();

var app = express();

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/admin');

var bodyParser = require('body-parser');

router.use(function timeLog(req, res, next){
  console.log("Time : ", Date.now());
  next();
  });
/*
var helmet = require('helmet');
app.use(helmet);
*/
app.use(bodyParser.urlencoded({
  extended : true
  }));

app.use(bodyParser.json());

app.use(session({
  secret: 'sadfaewaadfgsdf',
  resave : false,
  saveUninitialized : true
  }));

app.use(express.static('./public'));

require('./routes/ath/route.js')(app);

app.get("/*", function (req, res) {
  res.sendFile("public/index.html", {
    root: __dirname
  });
});

var server = app.listen(8080, function(){
  var host = "localhost";
  var port = server.address().port;
  console.log('App listening at http://%s:%s', host, port);
  
  });













